# generate .key
openssl genrsa -out banuka.key 2048

# generate .csr
openssl req -new \
    -key banuka.key \
    -out banuka.csr \
    -subj "/CN=banuka/O=jlabs"

ls ~/.minikube/
# Check that the files ca.crt and ca.key exist in the location.

# generate .crt
openssl x509 -req \
    -in banuka.csr \
    -CA /etc/kubernetes/pki/ca.crt \
    -CAkey /etc/kubernetes/pki/ca.key \
    -CAcreateserial \
    -out banuka.crt \
    -days 500

kubectl config set-credentials banuka \
    --client-certificate=banuka.crt \
    --client-key=banuka.key

kubectl config set-context banuka-context \
    --cluster=kubernetes \
    --namespace=default \
    --user=banuka

kubectl config view

kubectl config use-context banuka-context

kubectl config current-context