# k8s-gcp-cicd

Provisioning a kubernetes cluster with terraform and ansible on GCP with the help of a user-defined gitlab runner and Gitlab CI/CD pipeline

This project is intended to spin up a `kubernetes` cluster with 

- 1 Master Node (e2-medium, 2vCPU, 4GB Memory)
- 2 Worker Nodes (e2-medium, 2vCPU, 4GB Memory)
- 1 NFS Server (e2-medium, 2vCPU, 4GB Memory) - __Optional__

> PS: Since this is a test cluster, I have used the same memory, disk and network resources for the Master as same as the worker nodes. You can change the machine type as per your needs but keep the `Master` specifications as-is or above.

**Once the cluster is Set up, following tools will be set up:**

- [x] **Monitoring**
  - [x] Installing Prometheus
  - [x] Installing Prometheus node_exporter
  - [x] Installing Grafana

This will install `Prometheus` and `Grafana` on **Master server** and `node_exporter` on each **Worker Node** to scrape the metrices. 

Both `Prometheus` and `Grafana` are running as `Docker` containers exposing their respective ports.

---

- [x] **NFS-Provisioner**
  - [x] Configure NFS Server
  - [x] Configure Nodes

A dynamic NFS Provisioner for `kubernetes` **Persistent Storage** to use by the pods. This will automatically spin up the Server and configure NFS provisioner with each node (Worker nodes) so they can use the mounted volumes in the deployments.

---

- [x] **Helm**
  - [x] Deploy Helm Chart

This will deploy a simple `helm` chart with `mongo-express` and `mongodb` images that is accessible through a `NodePort` type service for the sake of the completion.

Find my helm repository here: [k8s-helm-mongo-db-express-chart](https://gitlab.com/iamdempa/k8s-helm-mongo-db-express-chart)

> PS: Here I have used a `Deployment` for `mongodb` instead of using a `StatefulSets` which is not recommended. Please use `StatefulSets` for Stateful Application. Since this is not mainly focused on `helm` I have used a `Deployment` object. (Will update the repo once `StatefulSets` are used)

---

# prerequisites

You need the following steps to be followed before deploying the `kubernetes` cluster

__1. A specific Gitlab Runner__

You need to have a Gitlab-runner deployed in the GCP infrastructure to carry out the build jobs. 

a) Spin up an `centos 07` virtual machine __(e2-micro)__ is enough to accomodate the gitlab-runner

b) Download the gitlab-runner binary

```
# For Debian/Ubuntu/Mint
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# For RHEL/CentOS/Fedora
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
```

then install the gitlab-runner

```
# For Debian/Ubuntu/Mint
sudo apt-get install gitlab-runner

# For RHEL/CentOS/Fedora
sudo yum install -y gitlab-runner
```

then add the runner to the __root__ privileges group 

```
//debian
sudo usermod -a -G sudo gitlab-runner

//centos
sudo usermod -a -G wheel gitlab-runner
```

then edit the `visudo` file to provide the root access with no password. type

```
sudo visudo
```

and add under the `sudoers`

```
gitlab-runner ALL=(ALL) NOPASSWD: ALL
```

then register the runner

```
sudo gitlab-runner register
```

Or you can either provision `gitlab-runner` with GCP instance group with instance templates. Create a GCP instance template by giving `start_script` as below. 

```
#!/bin/sh
sudo yum update -y 
sudo yum install ansible -y
sudo su -
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
yum update -y
yum install gitlab-runner -y
usermod -a -G wheel gitlab-runner / usermod -aG sudo username
sh -c "echo \"gitlab-runner ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers"
export CI_SERVER_URL=https://gitlab.com/
export RUNNER_NAME=banuka
export REGISTRATION_TOKEN=<PROJECT_TOKEN>
export REGISTER_NON_INTERACTIVE=true
export RUNNER_EXECUTOR=shell
export RUNNER_TAG_LIST=<TAG_LIST>
gitlab-runner register
gitlab-runner uninstall
gitlab-runner install --working-directory /root --user root
service gitlab-runner restart
echo "hi"
```
Above, the value you specify for `RUNNER_TAG_LIST` should be the name you refer in the `tags` field in your `.gitlab-ci.yml`. And provide the `REGISTRATION_TOKEN` as well. You can find this token under the __Settings__ of your repo.

~ ~









